import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-newhome',
  templateUrl: './newhome.component.html',
  styleUrls: ['./newhome.component.css']
})
export class NewhomeComponent implements OnInit {
  requests: any = [];
  public searchbox = "";
  public category = ""; 
  public branch = "";
  request: any = {};
  pager:any  = {};
  pageOfItems:any = [];
  x:any = [];
  nextPage : number;
  pagenumbers:any;
  apiEndPoint = environment.apiUrl;
  imageUrl = environment.imageUrl;

  constructor(private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    localStorage.removeItem('xyz');
    localStorage.removeItem('authorname');
    localStorage.removeItem('actualprice');
    localStorage.removeItem('category');
    localStorage.removeItem('isdnnumber');
    localStorage.removeItem('edition');
    localStorage.removeItem('postBookId');
    localStorage.removeItem('sellingprice');
    localStorage.removeItem('department');
    localStorage.removeItem('bookname');
    let useridentify = localStorage.getItem('checkuser');
    if(useridentify==='1')
    { 
    this.loadPage(1,this.searchbox,this.category,this.branch);
  }
  else{
    this.router.navigateByUrl('');
  }
  }

  loadPage(pagenumber,searchbox,category, branch){
    this.pagenumbers = pagenumber;
  
    if(searchbox==""&& category==""&& branch=="")
    {
      this.spinner.show();
      this.http.get(this.apiEndPoint +'getBookDetails/'+pagenumber).subscribe(
        (x: any) => {
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      );
    }
    else{
      if(searchbox==""&& category!=""&& branch=="")
      {
        this.spinner.show();
        this.http.get(this.apiEndPoint +'getCategoryDetails/'+category+'/'+this.pagenumbers).subscribe(
          (x:any)=>{
            this.spinner.hide();
           this.pager = x.pager;
           this.pageOfItems = x.pageOfItems;
           this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )
      }else if(searchbox!=""&& category==""&& branch==""){
        this.spinner.show();
        this.http.get(this.apiEndPoint +'searchitem/'+searchbox+'/'+this.pagenumbers).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
           
            this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )
      }else if(searchbox==""&& category==""&& branch!=""){
       
        this.spinner.show();
        this.http.get(this.apiEndPoint +'getBranchDetails/'+branch+'/'+this.pagenumbers).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
           
            this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )
      }
      else{
        this.spinner.show();
        this.http.get(this.apiEndPoint +'searchitemWithAll/'+searchbox+'/'+this.pagenumbers+'/'+this.category+'/'+this.branch).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
           
            this.nextPage = parseInt(this.pager.currentPage)+1;
            
          }
        )  
      }
     
    }
  
  }
  searchItem(searchData)
  {
    let _userid = localStorage.getItem('userid');
    let pagenumber1 = this.pagenumbers;
    let pagenumber = 1;
    this.searchbox = searchData;
    if(searchData==""){
      this.loadPage(1,this.searchbox,this.category,this.branch);
    }
    else{
      if(searchData!=""&& this.category==""&& this.branch=="")
      {
        this.spinner.show();
        this.http.get(this.apiEndPoint +'searchitem/'+searchData+'/'+pagenumber1).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
           
            this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )
      }else{
        this.spinner.show();
        this.http.get(this.apiEndPoint +'searchitemWithAll/'+searchData+'/'+this.pagenumbers+'/'+this.category+'/'+this.branch).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
           
            this.nextPage = parseInt(this.pager.currentPage)+1;
            
          }
        )
      }
      
    }
  
  }
  categoryItem(category){
    if(category==""){
      this.spinner.show();
      this.http.get(this.apiEndPoint +'getBookDetails/'+this.pagenumbers).subscribe(
        (x: any) => {
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      );
    }else{
      this.spinner.show();
      this.http.get(this.apiEndPoint +'getCategoryDetails/'+category+'/'+this.pagenumbers).subscribe(
        (x:any)=>{
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      )
    }
  
  }

  branchItem(branch){
    if(branch=="")
    {
      this.spinner.show();
      this.http.get(this.apiEndPoint +'getBookDetails/'+this.pagenumbers).subscribe(
        (x: any) => {
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      );
    }else{
      this.spinner.show();
      this.http.get(this.apiEndPoint +'getBranchDetails/'+branch+'/'+this.pagenumbers).subscribe(
        (x:any)=>{
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      )
    }
  
  }

  buyBookDetail(val){
    localStorage.setItem("postBookId",val);
   // this.router.navigateByUrl('account');
  }

}
