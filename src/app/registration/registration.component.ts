import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { MustMatch } from '../_helpers/must-match.validator';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  userForm: FormGroup;
  submitted = false;
  apiEndPoint = environment.apiUrl;
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(3)]],
      rpassword: ['', [Validators.required, Validators.minLength(3)]],
      mbnumber: [, [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      nickname: ['', [Validators.required]],
    }, {
        validator: MustMatch('password', 'rpassword')
      }
    )
  }
  get f() { return this.userForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.userForm.valid) {
      this.spinner.show();
      this.http.post(this.apiEndPoint +'userRegistration', this.userForm.value).subscribe((response) => {
        this.spinner.hide();
        if (response['status'] === true) {
          Swal.fire(response['message']);
        this.router.navigateByUrl('login');
         
        }
        else {
          Swal.fire(response['message']);
          this.ngOnInit();
        }
        
      })
    }
    // else {
    //   alert('Your from is not valid');
    // }
  }

}
