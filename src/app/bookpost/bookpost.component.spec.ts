import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookpostComponent } from './bookpost.component';

describe('BookpostComponent', () => {
  let component: BookpostComponent;
  let fixture: ComponentFixture<BookpostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookpostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookpostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
