import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-buybookdetail',
  templateUrl: './buybookdetail.component.html',
  styleUrls: ['./buybookdetail.component.css']
})
export class BuybookdetailComponent implements OnInit {
  data:any;
  tempdata:any=[];
  apiEndPoint = environment.apiUrl;
  constructor(private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('checkuser');
    if(useridentify==='1')
    { 
      let userid = localStorage.getItem('userid');
      this.spinner.show();
      this.http.get(this.apiEndPoint + 'buyBookDetails/'+userid).subscribe((response)=>{
        this.spinner.hide();
        this.data = response;
      this.tempdata = this.data.doc;
      })
  }
  else{
    this.router.navigateByUrl('');
  }
  }

  cancelOrder(){
    Swal.fire({ title: 'To cancel your order send us a mail', html: 'with your Phone number,Book name, Auther name, ISBN number and Reason at<br>secondhandbookstore.info@gmail.com' });
  }

}
