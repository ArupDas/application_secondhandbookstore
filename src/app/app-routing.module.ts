import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { BookpostComponent } from './bookpost/bookpost.component';
import { NewhomeComponent } from './newhome/newhome.component';
import { LogoutComponent } from './logout/logout.component';
import { AccountComponent } from './account/account.component';
import { BuybookComponent } from './buybook/buybook.component';
import { BuybookdetailComponent } from './buybookdetail/buybookdetail.component';
import { SellbookdetailComponent } from './sellbookdetail/sellbookdetail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BuybookpaymentComponent } from './buybookpayment/buybookpayment.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';


const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'registration',
    component:RegistrationComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'bookpost',
    component:BookpostComponent
  },
  {
    path:'newhome',
    component:NewhomeComponent
  },
  {
    path:'logout',
    component:LogoutComponent
  },
  {
    path:'account',
    component:AccountComponent
  },
  {
    path:'buybook',
    component:BuybookComponent
  },
  {
    path:'buybookdetail',
    component:BuybookdetailComponent
  },
  {
    path:'sellbookdetail',
    component:SellbookdetailComponent
  },
  {
    path:'buybookpayment',
    component:BuybookpaymentComponent
  },
  {
    path:'editbook',
    component:EditBookComponent
  },
  {
    path:'forgetPassword',
    component:ForgetPasswordComponent
  },
  {
    path:'**',
    component:PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
