import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  requests: any = [];
  public searchbox = "";
  public category = "";
  public branch = "";
  request: any = {};
  pager:any  = {};
  pageOfItems:any = [];
  x:any = [];
  nextPage : number;
  pagenumbers:any;
  startdateData:any;
  enddateData:any;
  daterange:any;
   public fromDate = "";
   public toDate = "";
   apiEndPoint = environment.apiUrl;
   imageUrl = environment.imageUrl;

  constructor(private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loadPage(1,this.searchbox,this.category,this.branch);
  }
  loadPage(pagenumber,searchbox,category, branch){
    //console.log(pagenumber);
    this.pagenumbers = pagenumber;
  
    if(searchbox==""&& category==""&& branch=="")
    {
      this.spinner.show();
      this.http.get(this.apiEndPoint + 'getBookDetails/'+pagenumber).subscribe(
        (x: any) => {
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
         
        }
      );
    }
    else{
      if(searchbox==""&& category!=""&& branch=="")
      {
        this.spinner.show();
        this.http.get(this.apiEndPoint + 'getCategoryDetails/'+category+'/'+this.pagenumbers).subscribe(
          (x:any)=>{
            this.spinner.hide();
           this.pager = x.pager;
           this.pageOfItems = x.pageOfItems;
           this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )
      }else if(searchbox!=""&& category==""&& branch==""){
        this.spinner.show();
        this.http.get(this.apiEndPoint + 'searchitem/'+searchbox+'/'+this.pagenumbers).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
           
            this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )
      }else if(searchbox==""&& category==""&& branch!=""){
        this.spinner.show();
        this.http.get(this.apiEndPoint + 'getBranchDetails/'+branch+'/'+this.pagenumbers).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
            this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )
      }
      else{
        this.spinner.show();
        this.http.get(this.apiEndPoint + 'searchitemWithAll/'+searchbox+'/'+this.pagenumbers+'/'+this.category+'/'+this.branch).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
            this.nextPage = parseInt(this.pager.currentPage)+1;
            
          }
        )  
      }
     
    }
  
  }
  searchItem(searchData)
  {
    let _userid = localStorage.getItem('userid');
    let pagenumber1 = this.pagenumbers;
    let pagenumber = 1;
    this.searchbox = searchData;
    if(searchData==""){
      this.loadPage(1,this.searchbox,this.category,this.branch);
    }
    else{
      if(searchData!=""&& this.category==""&& this.branch=="")
      {
        this.spinner.show();
        this.http.get(this.apiEndPoint + 'searchitem/'+searchData+'/'+pagenumber1).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
           
            this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )
      }else{
        this.spinner.show();
        this.http.get(this.apiEndPoint + 'searchitemWithAll/'+searchData+'/'+this.pagenumbers+'/'+this.category+'/'+this.branch).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
           
            this.nextPage = parseInt(this.pager.currentPage)+1;
           
          }
        )
      }
      
    }
  
  }
  categoryItem(category){
    if(category==""){
      this.spinner.show();
      this.http.get(this.apiEndPoint + 'getBookDetails/'+this.pagenumbers).subscribe(
        (x: any) => {
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      );
    }else{
      this.spinner.show();
      this.http.get(this.apiEndPoint + 'getCategoryDetails/'+category+'/'+this.pagenumbers).subscribe(
        (x:any)=>{
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      )
    }
  
  }

  branchItem(branch){
    
    if(branch=="")
    {
      this.spinner.show()
      this.http.get(this.apiEndPoint + 'getBookDetails/'+this.pagenumbers).subscribe(
        (x: any) => {
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      );
    }else{
      this.spinner.show();
      this.http.get(this.apiEndPoint + 'getBranchDetails/'+branch+'/'+this.pagenumbers).subscribe(
        (x:any)=>{
          this.spinner.hide();
         this.pager = x.pager;
         this.pageOfItems = x.pageOfItems;
         this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      )
    }
  
  }

}

