import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  data1: Object;
  id: any;
  submitted = false;
  apiEndPoint = environment.apiUrl;
  constructor(private router: Router, private formBuilder: FormBuilder, private http: HttpClient,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    localStorage.clear();
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });
  }
  get f() { return this.loginForm.controls; }
  onSubmit() {
    this.submitted = true; 
    if (this.loginForm.valid) {
      this.spinner.show();
      this.http.post(this.apiEndPoint +'userLogin', this.loginForm.value)
        .subscribe((response) => {
          this.spinner.hide();
          if (response['status'] === true) {
            this.id = response['result']._id;
            localStorage.setItem('checkuser', '1');
            localStorage.setItem("userid", this.id);
            this.router.navigateByUrl('newhome');
          }
          else {
            Swal.fire(response['message']);
            this.ngOnInit();
          } 
        }) 
    }
    // else {
    //   alert('invalid Entries');
    // }
  }
}
