import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { MustMatch } from '../_helpers/must-match.validator';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {
  postUserForm: FormGroup;
  submitted = false;
  public imagePath;
  imgURL: any;
  public frontbookimage:any;
  apiEndPoint = environment.apiUrl;
  imageUrl = environment.imageUrl
  ImageFile:any = null;
  bookId:any = null;
  bookImage:any = null;
 

  constructor(private formBuilder: FormBuilder, private route:ActivatedRoute,private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('checkuser');
    this.route.queryParams.subscribe(params =>{
      this.bookId = params.bookId;
    })
    if(useridentify==='1')
    { 
     this.formInilization();
     this.setValue();
    }
    else{
      Swal.fire("Please first Login, Then Post a Book");
      this.router.navigateByUrl('login');
    }
  } 
  setValue(){
    let postbookid = this.bookId;
    this.http.get(this.apiEndPoint + 'getBuyBookDetail/'+postbookid).subscribe((response:any) => {
      this.spinner.hide();
      let data = response.doc;
      this.bookImage = data.frontbookimage;
      this.postUserForm.patchValue({
        category:data.category,
        department:data.department,
        bookname:data.bookname,
        authorname:data.authorname,
        isdnnumber:data.isdnnumber,
        Cisdnnumber:data.isdnnumber,
        edition:data.edition,
        actualprice:data.actualprice,
        sellingprice:data.sellingprice,
        description:data.description,
        name:data.name,
        email:data.email,
        mbnumber:data.mbnumber,
        state:data.state,
        city:data.city,
        pickaddress:data.pickaddress,
        pincode:data.pincode,
        upi:data.upi
      })

    })
  }

  formInilization(){
    this.postUserForm = this.formBuilder.group({
      category: ['', [Validators.required]],
      department: ['', [Validators.required]],
      bookname: ['', [Validators.required]],
      authorname: ['',],
      isdnnumber: ['', [Validators.required]],
      Cisdnnumber: ['', [Validators.required]],
      edition: ['',],
      actualprice: ['', [Validators.required]],
      sellingprice: ['', [Validators.required]],
      description: ['', [Validators.required]],
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      mbnumber: [, [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      state: ['', [Validators.required]],
      city: ['', [Validators.required]],
      pickaddress: ['', [Validators.required]],
      pincode: ['', [Validators.required]],
      upi: ['', [Validators.required]],
    }, {
      validator: MustMatch('isdnnumber', 'Cisdnnumber')
    }
    )
  }
  get f() { return this.postUserForm.controls; }
   
  onSubmit() {
    this.submitted = true;
    if (this.postUserForm.valid) {
      this.postUserForm.value.postbookid = this.bookId;
      this.spinner.show();
      this.http.post(this.apiEndPoint + 'editBookDetail', this.postUserForm.value).subscribe((response) => {
        this.spinner.hide();
        Swal.fire('You Update Successfully');
       this.router.navigateByUrl('sellbookdetail');
      })
    }
  }

  isbnExample()
  {
    Swal.fire({
      title: 'You can find ISBN No. to the back cover of your book or 1st, 2nd page from front side',
      imageUrl: 'assets/images/isbnImage.jpg',
      imageHeight: 200,
      imageWidth:200
      
    })
  }
}
