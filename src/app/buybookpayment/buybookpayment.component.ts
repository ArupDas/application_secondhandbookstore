import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-buybookpayment',
  templateUrl: './buybookpayment.component.html',
  styleUrls: ['./buybookpayment.component.css']
})
export class BuybookpaymentComponent implements OnInit {

  bookdata: any = [];
  paytm = [];
  apiEndPoint = environment.apiUrl;
  imageUrl = environment.imageUrl;
  totalPayment:any;
  gstTax = 2;
  constructor( private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('checkuser');

    if (useridentify === '1') {
      let postbookid = localStorage.getItem('postBookId');
      this.spinner.show();
      this.http.get(this.apiEndPoint +'getBuyBookDetail/' + postbookid).subscribe((response: any) => {
        this.spinner.hide();
        this.bookdata = response.doc;
        this.totalPayment =  Number(this.bookdata.sellingprice) + this.gstTax;
      })
    }
    else {
      Swal.fire("please first login to buy book");
      this.router.navigateByUrl('login');
    }
  }
  
  payment(val){
    localStorage.removeItem('paymentMode');
    this.spinner.show();
    let amount = this.totalPayment;
    this.http
    .get("http://localhost:3000/paywithpaytm/" + amount)
    .subscribe((res: any) => {   
      this.spinner.hide(); 
      this.paytm = res.result;
      // than i will create form
      this.createPaytmForm();
    });
  }

  createPaytmForm() {
    localStorage.setItem('xyz','6');
    localStorage.setItem('totalPayment',this.totalPayment);
    let gstPrice = String(this.gstTax);
    localStorage.setItem('gstTax',gstPrice);
    const my_form: any = document.createElement("form");
    my_form.name = "paytm_form";
    my_form.method = "post";
    my_form.action = "https://securegw-stage.paytm.in/theia/processTransaction";

    const myParams = Object.keys(this.paytm);
    for (let i = 0; i < myParams.length; i++) {
      const key = myParams[i];
      let my_tb: any = document.createElement("input");
      my_tb.type = "hidden";
      my_tb.name = key;
      my_tb.value = this.paytm[key];
      my_form.appendChild(my_tb);
    }

    document.body.appendChild(my_form);
    my_form.submit();
    // after click will fire you will redirect to paytm payment page.
    // after complete or fail transaction you will redirect to your CALLBACK URL
  }

  cod(){
    localStorage.setItem('xyz','6');
    localStorage.setItem('paymentMode','9');
    localStorage.setItem('totalPayment',this.totalPayment);
    let gstPrice = String(this.gstTax);
    localStorage.setItem('gstTax',gstPrice);
    this.router.navigateByUrl('buybook');
  }

}
