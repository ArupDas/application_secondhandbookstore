import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  data:any;
  tempdata:any;
  apiEndPoint = environment.apiUrl;
  constructor(private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }
  
  ngOnInit() {
    let useridentify = localStorage.getItem('checkuser');
    if(useridentify==='1')
    { 
      let userid = localStorage.getItem('userid');
      this.spinner.show();
      this.http.get(this.apiEndPoint + 'userAccountDetails/'+userid).subscribe((response)=>{
        this.spinner.hide();
        this.data = response;
      this.tempdata = this.data.doc;
      })
  }
  else{
    this.router.navigateByUrl('');
  }
  }

  buybookdetail()
  {
    this.router.navigateByUrl('buybookdetail');
  }

  sellbookdetail()
  {
    this.router.navigateByUrl('sellbookdetail');
  }

}
