// export const environment = {
//   production: false,
//   apiUrl:'http://103.133.215.81:3000/api/'
// }; 


export const environment = {
  production: false,
  apiUrl:'http://localhost:3000/api/',
  imageUrl:'https://secondhandbookimage.s3.amazonaws.com/'
};